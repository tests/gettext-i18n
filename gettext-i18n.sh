#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"
. "${TESTDIR}"/common/update-test-path

failed_cnt=0
echo "Testing supported locales:"
for LOCALE in $(grep -v \# ${TESTDATADIR}/po/LINGUAS); do
    #Generate mo file first
    LOCALEDIR="${TESTDATADIR}/po/$LOCALE/LC_MESSAGES"
    mkdir -p "$LOCALEDIR"
    msgfmt -c -o "$LOCALEDIR"/test-gettext.mo po/$LOCALE.po

    OUTPUT=`LC_ALL=$LOCALE.utf8 TEXTDOMAINDIR=${TESTDATADIR}/po \
        gettext test-gettext C`
    echo -n $LOCALE.utf8
    if [ "$OUTPUT" != "$LOCALE" ]; then
        echo ": fail"
        failed_cnt=$(($failed_cnt+1))
    else
        echo ": pass"
    fi
done

if [ $failed_cnt -gt 0 ]; then
    echo "$failed_cnt test(s) failed"
else
    echo "All tests passed"
fi
exit $failed_cnt
